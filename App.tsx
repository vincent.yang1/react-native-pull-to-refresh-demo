import {autobind} from "core-decorators";
import React, {Component} from "react";
import {SafeAreaView, ScrollView, StyleSheet, Text, View, Animated, Platform} from "react-native";
import {PullToRefreshView} from "react-native-smooth-pull-to-refresh";
import {RefreshView} from "./RefreshView";

interface AppProps {
}

interface AppState {
  title: string;
  isRefreshing: boolean;
}

export class App extends Component<AppProps, AppState> {

  public state: AppState = {
    title: "Pull down to refresh",
    isRefreshing: false,
    fadeAnim: new Animated.Value(0),
    scaleAnim: new Animated.Value(0),

  };

  public render() {
    return (
      <SafeAreaView style={styles.container}>
        <View style={{height: 64, backgroundColor: "#24bdd8"}}>
          <Text style={{top: 35, fontWeight: "bold", fontSize: 18, color: "white", textAlign: "center"}}>Header</Text>
        </View>
        <PullToRefreshView
          minPullDistance={70}
          pullAnimHeight={70}
          onScrollEvent={this.onScrollEvent}
          onScroll={this.onScrollEvent}

          pullAnimYValues={{from: -50, to: 10}}
          isRefreshing={this.state.isRefreshing}
          onRefresh={this.onInnerRefresh}
          onTriggerToRefresh={this.onTriggerToRefresh}
          contentComponent={
            <ScrollView>
              <Text style={styles.block1}>BLOCK 1</Text>
              <Text style={styles.block2}>BLOCK 2</Text>
              <Text style={styles.block3}>BLOCK 3</Text>
            </ScrollView>
          }
        >
          <RefreshView title={this.state.title} fadeAnim={this.state.fadeAnim} scaleAnim={this.state.scaleAnim}/>
        </PullToRefreshView>
      </SafeAreaView>
    );
  }

  @autobind
  private onInnerRefresh() {
    this.setState({title: "Loading..."});
    this.startRefreshing();
  }

  @autobind
  private onTriggerToRefresh(triggered: boolean) {
    this.setState({title: triggered ? "Release to refresh" : "Pull down to refresh"});
  }

  onScrollEvent = (event) => {
    console.log(event)
    //this.setState({
      //scrollY: event.nativeEvent.contentOffset.y
    //})
    let y = Math.abs(event.nativeEvent.contentOffset.y);
    if(Platform.OS == 'android') {
      y = y - 70
    }
    let toValue = (y) / 70 < 1 ?  y / 70 : 1
    console.log(toValue)
    console.log(event.nativeEvent.contentOffset.y)
    toValue = Math.abs(toValue)

    
    Animated.parallel([
      Animated.timing(this.state.fadeAnim, {
        toValue: toValue,
        duration:0,
        useNativeDriver: true
      }),
      Animated.timing(
        this.state.scaleAnim,
        {
          toValue: toValue,
          duration: 0,
          useNativeDriver: true
        }
      ),


    ]).start(() => {
      // callback

    });

  }

  private startRefreshing() {
    this.setState({
      isRefreshing: true,
    });
    setTimeout(() => {
      this.setState({isRefreshing: false});
    }, 1500);
  }

  private stopRefreshing() {
    this.setState({isRefreshing: false});
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "white",
  },
  block1: {
    margin: 2,
    fontSize: 20,
    textAlign: "center",
    lineHeight: 230,
    height: 230,
    backgroundColor: "#9b9287",
  },
  block2: {
    margin: 2,
    fontSize: 20,
    textAlign: "center",
    lineHeight: 230,
    height: 230,
    backgroundColor: "#9b9287",
  },
  block3: {
    margin: 2,
    fontSize: 20,
    textAlign: "center",
    lineHeight: 230,
    height: 230,
    backgroundColor: "#9b9287",
  },
});
